package fromtests;

import java.sql.*;

/**
 * Created by andriy on 2/25/17.
 */
public class SelectDemoTest {
    public static void main( String args[] )
    {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/employees","andriy","pa$$w0rd");
//            connection = DriverManager.getConnection("jdbc:sqlite:test3.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try  (Statement statement = connection.createStatement();

              ResultSet resultSet = statement.executeQuery("SELECT * FROM contact"))  {
//            while (resultSet.next()) {
//                resultSet.next();
                System.out.println(resultSet.getInt("id") + "\t"

                        + resultSet.getString("firstName") + "\t"

                        + resultSet.getString("lastName") + "\t"

                        + resultSet.getString("email") + "\t"

                        + resultSet.getString("phoneNo"));
//            }

        }   catch (SQLException sqle) {

            System.out.println("SQLException");
            sqle.printStackTrace();

        }
    }
}
