package fromtests;
import java.sql.*;

/**
 * Created by andriy on 2/25/17.
 */
public class SelectDemoTest3 {
    public static void main( String args[] )
    {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:test3.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try  {

            String query = "SELECT * FROM Item WHERE ID=110";

            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next ()) {

                System.out.println("ID: " + rs.getInt("Id"));

                System.out.println("Description: " + rs.getString("Descrip"));

                System.out.println("Price: " + rs.getDouble("Price"));

                System.out.println("Quantity: "+ rs.getInt("Quantity"));

            }

        }  catch (SQLException se) {

            System.out.println("Error");
            se.printStackTrace();

        }
    }
}
