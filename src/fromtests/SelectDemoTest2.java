package fromtests;

import java.sql.*;

/**
 * Created by andriy on 2/25/17.
 */
public class SelectDemoTest2 {
    public static void main( String args[] )
    {
        Connection conn = null;
        try {
//            conn = DriverManager.getConnection("jdbc:sqlite:test3.db");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/employees","andriy","pa$$w0rd");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String query = "SELECT ID FROM Employee";

        try  (Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(query);

            stmt.executeQuery ("SELECT ID FROM Customer");
//            stmt.executeQuery ("SELECT ID FROM contact");

            while (rs.next())  {
                System.out.println ("Employee ID: " + rs.getInt("ID") );
            }

        }  catch (Exception e) {

            System.out.println ("Error");
            e.printStackTrace();

        }
    }
}
