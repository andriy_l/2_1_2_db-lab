package fromtests;
import java.sql.*;

/**
 * Created by andriy on 2/25/17.
 */
public class SelectDemoTest4 {
    public static void main( String args[] )
    {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:test3.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try  {

            String query = "SELECT * FROM Employee WHERE ID=110";

            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery(query);                                // Line1

            System.out.println("Employee ID: " + rs.getInt("ID"));              // Line2

        }  catch (Exception se)  {

            System.out.println("Error");

        }
    }
}
