import java.sql.*;

/**
 * Created by andriy on 17.07.17.
 */
public class Monday {
    public static void main(String[] args) {
        // 0. в classpath додати jar-файли
        // 1. create connection
        String sqlDrop = "DROP TABLE IF EXISTS students";
        String sql = "CREATE TABLE IF NOT EXISTS students (" +
                "id INTEGER NOT NULL UNIQUE  AUTO_INCREMENT," +
                "first_name varchar(80) NOT NULL DEFAULT \'Ivan\'," +
                "last_name varchar(80) NOT NULL DEFAULT \'Mazepa\'," +
                "age int(3) DEFAULT 25)";

        String sqlInsert = "INSERT INTO students(first_name,last_name,age) " +
                "VALUES(\'Petro\',\'Sagaydachnyy\', 45) ";
        String sqlInsert5 = "INSERT INTO students(first_name,last_name,age) " +
                "VALUES(\'Dmytro\',\'Doroshenko\', 45) ";
        String sqlInsert6 = "INSERT INTO students(first_name,last_name,age) " +
                "VALUES(\'Petro\',\'Poroshenko\', 45) ";
        String sqlInsert2 = "INSERT INTO students(first_name,last_name,age) " +
                "VALUES(\'Dmytro\',\'Vyshnivetskyy\', 35) ";
        String sqlInsert3 = "INSERT INTO students() VALUES()";

        String sqlSelect1 = "SELECT * FROM students";
        String sqlSelect2 = "SELECT * FROM students WHERE last_name NOT LIKE \'%shenko\'";
        String sqlSelect3 = "SELECT * FROM students WHERE age > 7";
        String sqlSelect4 = "SELECT first_name FROM students WHERE age > 10";
        String sqlSelect5 = "SELECT first_name FROM students WHERE age > 10";
        String sqlIndex = "CREATE INDEX ididx ON students(id) ";
        String sqlCreateNewTable = "CREATE TABLE IF NOT EXISTS new_tbl AS SELECT * FROM students;";

        String prepStat = "INSERT INTO students(first_name,last_name,age) VALUES(?,?,?) ";
        try(
                Connection connection = DriverManager.getConnection(
                "jdbc:mysql://192.168.1.174:3306/guestdb" +
                        "?useUnicode=true" +
                        "&characterEncoding=UTF-8" +
                        "&useLegacyDatetimeCode=false" +
                        "&serverTimezone=UTC" +
                        "&useSSL=false",
                        "guest","123");
//                Connection connection = DriverManager.getConnection("jdbc:sqlite:hetmans.db");
        Statement statement = connection.createStatement();){
            connection.setAutoCommit(false);
            System.out.println("connected");

            System.out.println(statement.execute(sqlDrop));
            System.out.println(statement.execute(sql));
            System.out.println(statement.execute(sqlInsert));
            System.out.println(statement.execute(sqlInsert2));
            System.out.println(statement.execute(sqlInsert3));
            System.out.println(statement.execute(sqlInsert5));
            System.out.println(statement.execute(sqlInsert6));
            System.out.println(statement.execute(sqlCreateNewTable));
            try(PreparedStatement preparedStatement = connection.prepareStatement(prepStat)){
                preparedStatement.setString(1, "Bohdan");
                preparedStatement.setString(2, "Khmelnytskyy");
                preparedStatement.setInt(3, 40);
                System.out.println(preparedStatement.executeUpdate());
            }

            System.out.println("created index: " + statement.execute(sqlIndex));

            try(PreparedStatement preparedStatement =
                        connection.prepareStatement(sqlSelect1)){
                System.out.println("execute: " + preparedStatement.execute());
            ResultSet resultSet = preparedStatement.getResultSet();
                ResultSetMetaData rsmd = resultSet.getMetaData();
//                int numberOfColumns = rsmd.

                    if(resultSet.absolute(3)) {
                        int id = resultSet.getInt("id");
                        String fName = resultSet.getString("first_name");
                        String lName = resultSet.getString("last_name");
                        int age = resultSet.getInt("age");
                        System.out.println(id + " | " + fName + " | " + lName + " | " + age);
                    }

//            while (resultSet.next()){
//                System.out.println(resultSet.getString("first_name"));
//            }
            }

            // DB metadata
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            System.out.println(databaseMetaData.getDatabaseProductName());
            System.out.println(databaseMetaData.getDatabaseProductVersion());
            System.out.println(databaseMetaData.getDefaultTransactionIsolation());
            System.out.println(databaseMetaData.getStringFunctions());

            connection.commit();
            connection.rollback();
        }catch (SQLException sqle){
            sqle.printStackTrace();

        }
    }
}
