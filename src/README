0. Get MySQL JDBC driver from official site or from maven repository.
    Add it to your CLASSPATH or to your project in IDE.
1. Create class with methods to create database "mainacademy".
[optional] (Create additional user to manage tables in  mainacademy database)
2. Create helper-class to create instance of Connection interface with static method getConnection(),
   which load database connection properties from database.properties file.
   Create database.properties with such fields but field your connection parameters:
db.driver = com.mysql.jdbc.Driver
db.user = andriy
db.password = pa$$
db.url = jdbc:mysql://localhost:3306/mainacademy?useUnicode=true&characterEncoding=UTF-8
db.useUnicode = true
db.encoding = UTF-8

3. Create class for creating tables in database mainacademy:

+-----------------------+
| Tables_in_mainacademy |
+-----------------------+
| cities                |
| courses               |
| instructors           |
| sgroup                |
| sgroup_instructors    |
| students              |
+-----------------------+

cities:
+----------+-------------+------+-----+--------------------+----------------+
| Field    | Type        | Null | Key | Default            | Extra          |
+----------+-------------+------+-----+--------------------+----------------+
| CITYID   | int(11)     | NO   | PRI | NULL               | auto_increment |
| CITYNAME | varchar(30) | NO   |     | Тернопіль          |                |
+----------+-------------+------+-----+--------------------+----------------+

courses:
+------------+--------------+------+-----+-----------------------------------------+----------------+
| Field      | Type         | Null | Key | Default                                 | Extra          |
+------------+--------------+------+-----+-----------------------------------------+----------------+
| COURSEID   | int(11)      | NO   | PRI | NULL                                    | auto_increment |
| COURSENAME | varchar(120) | NO   |     | Основи програмування                    |                |
| HOURS      | int(11)      | NO   |     | 24                                      |                |
| PROJECT    | tinyint(1)   | YES  |     | 0                                       |                |
| PRICE      | double       | YES  |     | NULL                                    |                |
+------------+--------------+------+-----+-----------------------------------------+----------------+

instructors:
+--------------+-------------+------+-----+-------------------------+----------------+
| Field        | Type        | Null | Key | Default                 | Extra          |
+--------------+-------------+------+-----+-------------------------+----------------+
| INSTRUCTORID | int(11)     | NO   | PRI | NULL                    | auto_increment |
| FIRSTNAME    | varchar(80) | NO   |     | Іван                    |                |
| SECONDNAME   | varchar(80) | NO   |     | Іваненко                |                |
| MIDDLENAME   | varchar(80) | NO   |     | Іванович                |                |
| EMAIL        | varchar(80) | NO   |     | instructor@mainacad.com |                |
| PHONE        | varchar(80) | NO   |     | +38098199-55-55         |                |
| PHOTO        | blob        | YES  |     | NULL                    |                |
| BIRTHDATE    | datetime    | NO   |     | CURRENT_TIMESTAMP       |                |
+--------------+-------------+------+-----+-------------------------+----------------+

Study groups - sgroup:
+-----------+----------+------+-----+-------------------+----------------+
| Field     | Type     | Null | Key | Default           | Extra          |
+-----------+----------+------+-----+-------------------+----------------+
| GROUPID   | int(11)  | NO   | PRI | NULL              | auto_increment |
| CITYID    | int(11)  | NO   | MUL | NULL              |                |
| COURSEID  | int(11)  | NO   | MUL | NULL              |                |
| STARTDATE | datetime | NO   |     | CURRENT_TIMESTAMP |                |
+-----------+----------+------+-----+-------------------+----------------+

students:
+------------+-------------+------+-----+------------------------+----------------+
| Field      | Type        | Null | Key | Default                | Extra          |
+------------+-------------+------+-----+------------------------+----------------+
| STUDENTID  | int(11)     | NO   | PRI | NULL                   | auto_increment |
| FIRSTNAME  | varchar(80) | NO   |     | Петро                  |                |
| SECONDNAME | varchar(80) | NO   |     | Голохвастов            |                |
| MIDDLENAME | varchar(80) | NO   |     | Васильович             |                |
| EMAIL      | varchar(80) | NO   |     | student@gmail.com      |                |
| PHONE      | varchar(80) | NO   |     | +38098111-22-33        |                |
| GROUPID    | int(11)     | NO   | MUL | NULL                   |                |
| AGE        | int(11)     | NO   |     | 25                     |                |
+------------+-------------+------+-----+------------------------+----------------+

sgroup_instructors:
+--------------+---------+------+-----+---------+-------+
| Field        | Type    | Null | Key | Default | Extra |
+--------------+---------+------+-----+---------+-------+
| GROUPID      | int(11) | NO   | MUL | NULL    |       |
| INSTRUCTORID | int(11) | NO   | MUL | NULL    |       |
+--------------+---------+------+-----+---------+-------+

Check correctness by CLI or GUI MySQL client.

4. Create classes to  fill database which uses classes:
  CityDAO with methods:
- to add/delete/update cities with public methods addCity() with String parameter cityName which must be unique,
deleteCity() and updateCity() with int parameter cityid,
- to find city id by name;
  InstructorDAO with methods:
- to add/delete instructors with public methods addInstructor() with parameters according to table structure,
deleteInstructor()  with int parameter instructorId [insertion into field photo is optional],
- to find instructors id by name;
 StudentDAO with methods:
- to add/deletestudents with public methods addStudent() with parameters according to table structure,
deleteStudent() with int parameter studentId,
- to find students id by name;
 CourseDAO with methods:
- to add/delete courses with public methods addCourse() with parameters according to table structure,
deleteCourse() with int parameter courseId,
- to find course id by name;
 GroupDAO with methods:
- to add/delete group with public methods addGroup() with parameters according to table structure,
deleteCourse() with int parameter courseId,
- to find group id by name;


5. Write class for filling database with fake data: create at least 5 cities, 
40 groups (one instructor can have multiple groups and deliver few courses), 5 courses,
25 instructors, at least 300 students.
Sugession: create arrays with first and second names, phones and another information and 
by randomly choosing this information fill fields of database tables.
Show ability to use INSERT and PreparedStatements.

Check correctness by CLI or GUI MySQL client.

6. Create class for altering tables in database mainacademy
by adding username and password fields to instructor and student tables.
Show ability to use ALTER.
And setting values of username and password.
Show ability to use UPDATE.
Username is concatenation of first name letter and last name.
Password equals to username.

Check correctness by CLI or GUI MySQL client.

7. Create class for run different types of select.
- calculate total amount of students;
SELECT count(STUDENTID) FROM students;
парні:
SELECT COUNT(*) FROM students WHERE (STUDENTID % 2) = 0;

- calculate total amount of salary of instructors:
SELECT SUM(salaries.salary) FROM salaries;
- display only managers:
SELECT employees.* ,dept_manager.dept_no FROM employees INNER JOIN dept_manager ON employees.emp_no=dept_manager.emp_no;
- display only managers with salaries:

- calculate amount of salary of managers:

CREATE VIEW view_name AS SELECT column1, column2, ... FROM table_name WHERE condition;
