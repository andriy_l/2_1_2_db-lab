package com.maincademy.db.lab01_02;

import java.sql.*;
import java.util.Enumeration;

public class CreateDB {

    public static void main(String[] args) {
         try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/?useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false","andriy","pa$$w0rd");
            Statement statement = connection.createStatement();){
            String sql = "CREATE DATABASE IF NOT EXISTS mainacademy CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci";
            statement.executeUpdate(sql);
            System.out.println("Database created successfully...");
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
}
