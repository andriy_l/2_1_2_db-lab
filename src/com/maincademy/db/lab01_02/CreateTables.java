package com.maincademy.db.lab01_02;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTables
{
    public static void main(String[] args)
    {
        try(Connection connection = ConnectorDB.getConnection();
            Statement  statement  = connection.createStatement()){
            System.out.println("Opened database successfully");

            // ! remember about order of creating tables with FK

            String sqlCreateCoursesTable =
                    "CREATE TABLE IF NOT EXISTS courses " +
                    "(COURSEID              INT PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT," +
                    " COURSENAME      VARCHAR(120) NOT NULL UNIQUE DEFAULT \'Основи програмування\', " +
                    " HOURS           INT     NOT NULL DEFAULT 24, " +
                    " PROJECT         BOOLEAN DEFAULT FALSE, " +
                    " PRICE           REAL)";
            statement.executeUpdate(sqlCreateCoursesTable);

            String sqlCreateCitiesTable =
                    "CREATE TABLE IF NOT EXISTS cities " +
                            "(CITYID          INT PRIMARY KEY     NOT NULL UNIQUE AUTO_INCREMENT," +
                            " CITYNAME        VARCHAR(30) NOT NULL UNIQUE DEFAULT \'Тернопіль\') ";
            statement.executeUpdate(sqlCreateCitiesTable);

            String sqlCreateInstructorsTable =
                    "CREATE TABLE IF NOT EXISTS instructors " +
                            "(INSTRUCTORID    INT PRIMARY KEY     NOT NULL AUTO_INCREMENT," +
                            " FIRSTNAME       VARCHAR(80)     NOT NULL DEFAULT \'Іван\'," +
                            " SECONDNAME      VARCHAR(80)     NOT NULL DEFAULT \'Іваненко\'," +
                            " MIDDLENAME      VARCHAR(80)     NOT NULL DEFAULT \'Іванович\'," +
                            " EMAIL           VARCHAR(80)     NOT NULL DEFAULT \'instructor@mainacad.com\'," +
                            " PHONE           VARCHAR(80)     NOT NULL DEFAULT \'+38098199-55-55\'," +
                            " PHOTO           BLOB," +
                            " BIRTHDATE       DATETIME NOT NULL DEFAULT NOW()," +
                            " CONSTRAINT      UC_Instructor UNIQUE (FIRSTNAME,SECONDNAME,MIDDLENAME) " +
                            ")";
                            // SQL constraints are used to specify rules for data in a table.
                            // we ensure insertion of unique instructors (unique by FIRSTNAME,SECONDNAME,MIDDLENAME)
                            // CURDATE() - Returns the current date
                            // A foreign key with cascade delete means that if a record in the parent table is deleted,
                            // then the corresponding records in the child table will automatically be deleted.
                            // This is called a cascade delete in SQL Server.
            statement.executeUpdate(sqlCreateInstructorsTable);

            String sqlCreateGroupTable =
                    "CREATE TABLE IF NOT EXISTS sgroup " +
                            "(GROUPID    INT PRIMARY KEY     NOT NULL AUTO_INCREMENT," +
                            " CITYID     INT NOT NULL ," +
                            " COURSEID   INT NOT NULL ," +
                            " STARTDATE       DATETIME NOT NULL DEFAULT NOW(), " +
                            " FOREIGN KEY (CITYID) REFERENCES cities (CITYID) ON DELETE CASCADE, "+
                            " FOREIGN KEY (COURSEID) REFERENCES courses (COURSEID) ON DELETE CASCADE," +
                            " CONSTRAINT UC_SGroup UNIQUE (CITYID,COURSEID,STARTDATE)) ";
            statement.executeUpdate(sqlCreateGroupTable);

            String sqlCreateStudentsTable =
                    "CREATE TABLE IF NOT EXISTS students " +
                            "(STUDENTID    INT PRIMARY KEY     NOT NULL AUTO_INCREMENT," +
                            " FIRSTNAME       VARCHAR(80)     NOT NULL DEFAULT \'Петро\'," +
                            " SECONDNAME      VARCHAR(80)     NOT NULL DEFAULT \'Голохвастов\'," +
                            " MIDDLENAME      VARCHAR(80)     NOT NULL DEFAULT \'Васильович\'," +
                            " EMAIL           VARCHAR(80)     NOT NULL DEFAULT \'student@gmail.com\'," +
                            " PHONE           VARCHAR(80)     NOT NULL DEFAULT \'+38098111-22-33\'," +
                            " GROUPID           INT     NOT NULL ," +
                            " AGE             INT NOT NULL DEFAULT 25, " +
            //                " FOREIGN KEY (CITYID) REFERENCES cities (CITYID) ON DELETE CASCADE, " + // try with FK which not exists
                            " FOREIGN KEY (GROUPID) REFERENCES sgroup (GROUPID) ON DELETE CASCADE," +
                            " CONSTRAINT UC_Student UNIQUE (FIRSTNAME,SECONDNAME,MIDDLENAME)) ";
            statement.executeUpdate(sqlCreateStudentsTable);

            // many - to - many
            String sqlCreateSGroupInstructorsTable =
                    "CREATE TABLE IF NOT EXISTS sgroup_instructors " +
                            "(GROUPID      INT NOT NULL," +
                            " INSTRUCTORID INT NOT NULL," +
                            " FOREIGN KEY (GROUPID) REFERENCES sgroup (GROUPID) ON DELETE CASCADE, " +
                            " FOREIGN KEY (INSTRUCTORID) REFERENCES instructors (INSTRUCTORID) ON DELETE CASCADE) ";

            statement.executeUpdate(sqlCreateSGroupInstructorsTable);


        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            e.printStackTrace();
            System.exit(0);
        }
        System.out.println("Table created successfully");
    }
}
