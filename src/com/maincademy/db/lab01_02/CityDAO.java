package com.maincademy.db.lab01_02;

import java.sql.*;

/**
 * Created by andriy on 3/10/17.
 */
public class CityDAO {

    public void addCity(String cityName){
        // not enough effective, we also use constraint unique
        if (isAlreadyAdded(cityName)){
        // City is already added
            return;
        }
        String sql = "INSERT INTO cities (CITYNAME) VALUES (?)";
        try(Connection connection = ConnectorDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement(sql)){
            statement.setString(1, cityName);
            statement.executeUpdate();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }

    public void deleteCityByName(String cityName){

        String sql = "DELETE FROM cities WHERE CITYNAME = (?)";
        try(Connection connection = ConnectorDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement(sql)){
            statement.setString(1, cityName);
            statement.executeUpdate();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }

    public void deleteCityById(int cityId){

        String sql = "DELETE FROM cities WHERE CITYID LIKE (?)";
        try(Connection connection = ConnectorDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement(sql)){
            statement.setInt(1, cityId);
            statement.executeUpdate();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }

    public int getCityId(String name){
        int cityId = -1;

    //  String sql = "SELECT cities.CITYID FROM mainacademy.cities WHERE CITYNAME LIKE (\'?\')"; <= incorrect,
    //  use like in next line
        String sql = "SELECT cities.CITYID FROM mainacademy.cities WHERE CITYNAME LIKE (?)";
        try(Connection connection = ConnectorDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement(sql)){

            statement.setString(1, name);

            try(ResultSet resultSet = statement.executeQuery()) {
                // if we use SQLite we don't need next line!!!
                // But with RDBMS we need it
                if(resultSet.next()) {
                    cityId = resultSet.getInt("CITYID");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cityId;
    }

    public void updateCity(int cityId, String cityName){
        String sql = "UPDATE cities set CITYNAME = (?) WHERE CITYID = (?)";
        try(Connection connection = ConnectorDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement(sql)){
            statement.setString(1, cityName);
            statement.setInt(2, cityId);
            statement.executeUpdate();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
        // helper method to check if city was already added
    private boolean isAlreadyAdded(String cityName){
        boolean result = false;
        String sql = "SELECT * FROM cities WHERE CITYNAME = \'"+cityName+"\'";
        try(Connection connection = ConnectorDB.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs  = statement.executeQuery(sql)){
            if (rs.next()){
                rs.getString(2).equals(cityName);
                result = true;
            } else {
                result = false;
            }
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            e.printStackTrace();
        }
        return result;
    }


}
