package com.maincademy.db.lab01_02;

import com.maincademy.db.lab01_02.model.City;

/**
 * Created by andriy on 3/9/17.
 */
public class Main {
    public static void main(String[] args) {
        City city = new City("Тернопіль");
        System.out.println(city);
        City city2 = new City("Харків");
        System.out.println(city2);
        City city3 = new City("Київ");
        System.out.println(city3);
        City city4 = new City("Львів");
        System.out.println(city4);
        City city5 = new City("Одеса");
        System.out.println(city5);


    }
}
