package com.maincademy.db.lab01_02.model;

import java.io.Serializable;

/**
 * Created by andriy on 3/4/17.
 */
public class Person implements Serializable{
    private String firstname;
    private String secondname;
    private String middlename;
    private String email;
    private String phone;
    private String username;
    private String password;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Person(){

    }

    public Person(String firstname, String secondname, String middlename, String email, String phone, String username, String password) {
        this.firstname = firstname;
        this.secondname = secondname;
        this.middlename = middlename;
        this.email = email;
        this.phone = phone;
        this.username = username;
        this.password = password;
    }

}
