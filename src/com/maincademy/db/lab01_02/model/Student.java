package com.maincademy.db.lab01_02.model;

/**
 * Created by andriy on 3/4/17.
 */
public class Student extends Person {
    private int age = 25;
    private int groupId;

    public Student(){
        this("Андрій", "Андрієнко", "Васильович", "a@gmail.com", "098233-33-32", "andriyenko","andriyenko", 25, 1);
    }

    public Student(int age, int groupId) {
        this.age = age;
        this.groupId = groupId;
    }

    public Student(String firstname, String secondname, String middlename, String email, String phone, String username, String password, int age, int groupId) {
        super(firstname, secondname, middlename, email, phone, username, password);
        this.age = age;
        this.groupId = groupId;
    }
}
