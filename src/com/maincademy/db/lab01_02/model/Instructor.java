package com.maincademy.db.lab01_02.model;

import com.maincademy.db.lab01_02.ConnectorDB;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;

/**
 * Created by andriy on 3/4/17.
 */
public class Instructor extends Person {
    public static void main(String[] args) {
        Instructor i = new Instructor();
        i.storeInstructor();
    }
    private Image photo;
    private Date birthdate;

    public Instructor(){
        this("Петро", "Сагайдачний", "Конашевич", "sagajdachnyy@c4.com", "098555-55-55", "sagajdachnyy", "sagajdachnyy", null, new Date());
    }

    public Instructor(Image photo, Date birthdate) {
        this.photo = photo;
        this.birthdate = birthdate;
    }

    public Instructor(String firstname, String secondname, String middlename, String email, String phone, String username, String password, Image photo, Date birthdate) {
        super(firstname, secondname, middlename, email, phone, username, password);
        this.photo = photo;
        this.birthdate = birthdate;
    }

    public void storeInstructor(){
        BufferedImage photo = null;
        try {
            photo = ImageIO.read(new File("photo.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String sql =
                "INSERT INTO instructors (FIRSTNAME, SECONDNAME, MIDDLENAME, EMAIL, PHONE, PHOTO, BIRTHDATE) VALUES(?, ?, ?, ?, ?, ?, ?)";
        try(Connection connection = ConnectorDB.getConnection();
            PreparedStatement stat  = connection.prepareStatement(sql)){
            Blob coverBlob = connection.createBlob();
            int offset = 1;
            OutputStream out = coverBlob.setBinaryStream(offset);
            ImageIO.write( photo, "JPG", out);

            stat.setString(1, this.getFirstname() );
            stat.setString(2, this.getSecondname() );
            stat.setString(3, this.getMiddlename() );
            stat.setString(4, this.getEmail() );
            stat.setString(5, this.getPhone() );
            stat.setBlob(6, coverBlob);
            java.util.Date myDate = new java.util.Date("04/03/2016");
            java.sql.Date sqlDate = new java.sql.Date(myDate.getTime());
            stat.setDate(7, sqlDate);
            stat.executeUpdate();



            System.out.println("Opened database successfully");

        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            e.printStackTrace();
            System.exit(0);
        }


    }


}
