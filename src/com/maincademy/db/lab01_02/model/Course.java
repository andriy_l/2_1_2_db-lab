package com.maincademy.db.lab01_02.model;

/**
 * Created by andriy on 3/4/17.
 */
public class Course {
    private int courseID;
    private String courseName;
    private int hours;
    private boolean haveProject;
    private double price;

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public boolean isHaveProject() {
        return haveProject;
    }

    public void setHaveProject(boolean haveProject) {
        this.haveProject = haveProject;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Course(int courseID, String courseName, int hours, boolean haveProject, double price) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.hours = hours;
        this.haveProject = haveProject;
        this.price = price;
    }
}
