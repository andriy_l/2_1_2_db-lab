package com.maincademy.db.lab01_02.model;

import java.util.Date;

/**
 * Created by andriy on 3/4/17.
 */
public class Group {
    private int groupID;
    private int cityID;
    private int courseID;
    private Date startDate;

    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Group(int groupID, int cityID, int courseID, Date startDate) {
        this.groupID = groupID;
        this.cityID = cityID;
        this.courseID = courseID;
        this.startDate = startDate;
    }
}
