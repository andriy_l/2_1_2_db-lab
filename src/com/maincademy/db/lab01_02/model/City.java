package com.maincademy.db.lab01_02.model;

import com.maincademy.db.lab01_02.ConnectorDB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andriy on 3/4/17.
 */
public class City {


    private int cityID;
    private String cityName;

    public City(String cityName) {
        this.cityName = cityName;
        storeCity(cityName);
    }

    public int getCityID() {
        int cityID = 0;
        String sql = "SELECT CITYID FROM cities WHERE CITYNAME=\'"+cityName+"\'";
        try(Connection connection = ConnectorDB.getConnection();
            Statement statement  = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql)){
            if(rs.next()) {
                cityID = rs.getInt(1);
            }
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        return cityID;
    }

    private void setCityID(int cityID){
        this.cityID = cityID;
    }

    public String getCityName() {
        return cityName;
    }


    private void storeCity(String cityName){

        if (isAlreadyAdded(cityName)){
        // City is already added
            return;
        }
        String sql = "INSERT INTO cities (CITYNAME) VALUES (?)";
        String sql2 = "SELECT CITYID FROM cities WHERE CITYNAME = '"+cityName+"\'";
        try(Connection connection = ConnectorDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement(sql)){
            statement.setString(1, this.cityName);
            statement.executeUpdate();
            ResultSet rs = statement.executeQuery(sql2);
            if(rs.next()){
                this.cityID = rs.getInt(1);
            }

        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }

    public static List<City> getCities(){
        List<City> cities = new ArrayList<>();
        String sql =
                "SELECT * FROM cities";
        try(Connection connection = ConnectorDB.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs  = statement.executeQuery(sql)){
                while (rs.next()) {
                    City c = new City(rs.getString(2));
                    c.setCityID(rs.getInt(1));
                    cities.add(c);
                }
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            e.printStackTrace();
            System.exit(0);
        }
        return cities;
    }

    private boolean isAlreadyAdded(String cityName){
        boolean result = false;
        String sql = "SELECT * FROM cities WHERE CITYNAME = \'"+cityName+"\'";
        try(Connection connection = ConnectorDB.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs  = statement.executeQuery(sql)){
            if (rs.next()){
                rs.getString(2).equals(cityName);
                this.cityID = rs.getInt(1);
                result = true;
            } else {
                result = false;
            }
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            e.printStackTrace();
            System.exit(0);
        }
        return result;
    }

    @Override
    public String toString() {
        return "City{" +
                "cityID=" + cityID +
                ", cityName='" + cityName + '\'' +
                '}';
    }

}
