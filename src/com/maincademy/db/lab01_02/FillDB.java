package com.maincademy.db.lab01_02;

/**
 * Created by andriy on 7/13/17.
 */
public class FillDB {
    public static void main(String[] args) {
        CityDAO cityDAO = new CityDAO();
        cityDAO.addCity("Тернопіль");
        cityDAO.addCity("Львів");
        cityDAO.addCity("Київ");
        cityDAO.addCity("Харків");
        cityDAO.addCity("Одеса");
        cityDAO.addCity("Дніпро");
        cityDAO.addCity("Ужгород");
        cityDAO.addCity("Бережани");
        cityDAO.addCity("БережИни");
        System.out.println("City Id is " + cityDAO.getCityId("Бережани"));
        cityDAO.updateCity(17, "Бережони");
        cityDAO.deleteCityById(17);
        cityDAO.deleteCityByName("Бережони");


    }
}
