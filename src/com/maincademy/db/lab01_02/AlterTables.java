package com.maincademy.db.lab01_02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/*
Add user and password fields to studens and instructor tables
 */
public class AlterTables {

    public static void main( String args[] )
    {
        try(Connection connection = ConnectorDB.getConnection();
            Statement  statement  = connection.createStatement()){
            System.out.println("Opened database successfully");

            String sqlAlterInstructorsTable =
                    "ALTER TABLE instructors" +
                            " ADD COLUMN IF NOT EXISTS username  VARCHAR(50)    NOT NULL," +
                            " ADD COLUMN IF NOT EXISTS password  VARCHAR(50)    NOT NULL";
            statement.executeUpdate(sqlAlterInstructorsTable);

            String sqlAlterStudentsTable =
                    "ALTER TABLE students" +
                            " ADD COLUMN username  VARCHAR(50)    NOT NULL," +
                            " ADD COLUMN password  VARCHAR(50)    NOT NULL";
            statement.executeUpdate(sqlAlterStudentsTable);


        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            e.printStackTrace();
            System.exit(0);
        }
        System.out.println("Table altered successfully");
    }
}
