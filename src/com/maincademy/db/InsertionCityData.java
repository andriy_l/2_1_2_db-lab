package com.maincademy.db;

import com.maincademy.db.model.City;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by andriy on 3/10/17.
 */
public class InsertionCityData {

    public void storeCity(String cityName){

        if (isAlreadyAdded(cityName)){
        // City is already added
            return;
        }
        String sql = "INSERT INTO cities (CITYNAME) VALUES (?)";
        try(Connection connection = ConnectorDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement(sql)){
            statement.setString(1, cityName);
            statement.executeUpdate();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }

    private boolean isAlreadyAdded(String cityName){
        boolean result = false;
        String sql = "SELECT * FROM cities WHERE CITYNAME = \'"+cityName+"\'";
        try(Connection connection = ConnectorDB.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs  = statement.executeQuery(sql)){
            if (rs.next()){
                rs.getString(2).equals(cityName);
                result = true;
            } else {
                result = false;
            }
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            e.printStackTrace();
            System.exit(0);
        }
        return result;
    }
}
