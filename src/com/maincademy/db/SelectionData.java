package com.maincademy.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by andriy on 3/12/17.
 */
public class SelectionData {

    public int getCityID(String cityName) {
        int cityID = 0;
        String sql = "SELECT CITYID FROM cities WHERE CITYNAME=\'"+cityName+"\'";
        try(Connection connection = ConnectorDB.getConnection();
            Statement statement  = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql)){
            if(rs.next()) {
                cityID = rs.getInt(1);
            }
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        return cityID;
    }
}
