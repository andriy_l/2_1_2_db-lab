package com.maincademy.db;

import java.sql.*;
import java.util.Enumeration;

public class CreateDB {

    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/","andriy","pa$$w0rd");
            System.out.println("Creating database...");
            statement = connection.createStatement();
            String sql = "CREATE DATABASE IF NOT EXISTS " +
                    "mainacademy CHARACTER SET utf8mb4 " +
                    "COLLATE utf8mb4_unicode_ci";
            statement.executeUpdate(sql);
            System.out.println("Database created successfully...");
        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        } finally {
                try {
                    if(statement != null)
                        statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            try {
                if(connection != null)
                    connection.close();
            } catch (SQLException e) {
                        e.printStackTrace();
            }
        }
    }
}
