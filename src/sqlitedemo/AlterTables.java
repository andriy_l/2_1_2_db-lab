package sqlitedemo;

import com.maincademy.db.lab01_02.ConnectorDB;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/*
Add user and password fields to studens and instructor tables
 */
public class AlterTables {

    public static void main( String args[] )
    {

        try(Connection connection = ConnectorDB.getConnection();
            Statement  statement  = connection.createStatement()){
            System.out.println("Opened database successfully");
            String sqlAlterInstructorsTable =
                    "ALTER TABLE instructors" +
                            " ADD COLUMN username  VARCHAR(50)    NOT NULL," +
                            " ADD COLUMN password  VARCHAR(50)    NOT NULL";
            statement.executeUpdate(sqlAlterInstructorsTable);

            String sqlAlterStudentsTable =
                    "ALTER TABLE students" +
                            " ADD COLUMN username  VARCHAR(50)    NOT NULL," +
                            " ADD COLUMN password  VARCHAR(50)    NOT NULL";
            statement.executeUpdate(sqlAlterStudentsTable);

            String sqlSetInstructorsName = "UPDATE instructors SET password = (username=(LEFT(FIRSTNAME, 1)+SECONDNAME))";
            statement.executeUpdate(sqlSetInstructorsName);
            String sqlSetStudentsName = "UPDATE students SET password = (username=(LEFT(FIRSTNAME, 1)+SECONDNAME))";
            statement.executeUpdate(sqlSetStudentsName);


        } catch ( SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            e.printStackTrace();
            System.exit(0);
        }
        System.out.println("Table created successfully");
    }
}
