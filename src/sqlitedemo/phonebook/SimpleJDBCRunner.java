package sqlitedemo.phonebook;

import sqlitedemo.phonebook.data.Abonent;

import java.sql.*;
import java.util.ArrayList;

public class SimpleJDBCRunner {
        public static void main(String[] args) {

//            String url = "jdbc:mysql://localhost:3306/testphones";
            String url = "jdbc:sqlite:testphones.db";

//            Properties prop = new Properties();
//            prop.put("user", "root");
//            prop.put("password", "pass");
//            prop.put("autoReconnect", "true");
//            prop.put("characterEncoding", "UTF-8");
//            prop.put("useUnicode", "true");

            Connection cn = null;
//            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            try { // 1 блок
                cn = DriverManager.getConnection(url);
//                cn = ConnectorDB.getConnection(); <--- правильно винести конфігурацію у файл.
                // і окремо її завантажувати

//                cn = DriverManager.getConnection(url, prop);
                DatabaseMetaData databaseMetaData = cn.getMetaData();
//                System.out.println(databaseMetaData.getD);
                System.out.println("DB vendor: " + databaseMetaData.getDatabaseProductName() +
                        " version: " + databaseMetaData.getDatabaseProductVersion());
                System.out.println("Drv Name: " + databaseMetaData.getDriverName());
                System.out.println("DB URL: " + databaseMetaData.getURL());
//                ResultSet tablesOfDB = databaseMetaData.getTables();

                Statement st = null;
                try { // 2 блок
                    st = cn.createStatement();
                    ResultSet rs = null;
                    ResultSetMetaData resultSetMetaData = null;
                    try { // 3 блок
                        rs = st.executeQuery("SELECT * FROM phonebook");
                        resultSetMetaData = rs.getMetaData();
                        System.out.println("Type of second field: " + resultSetMetaData.getColumnTypeName(2));

                        ArrayList<Abonent> lst = new ArrayList<>();
                        while (rs.next()) {
                            int id = rs.getInt(1);
                            int phone = rs.getInt(3);
                            String name = rs.getString(2);
                            lst.add(new Abonent(id, phone, name));
                        }

                        if (lst.size() > 0) {
                            System.out.println(lst);

                        } else {
                            System.out.println("Not found");
                        }
                    } finally { // для 3-го блока try
					/*

 * закрти ResultSet, якщо він був відкритим
 * якщо помилка сталась під час
 * читання з нього даних
					 */
                        if (rs != null) { // чи був створений ResultSet
                            rs.close();
                        } else {
                            System.err.println("error reading while reading from DB");
                        }
                    }
                } finally {
				/*

 * закрити,Statement якщо він був відкритий або помилка
* Відбулася під час створення Statement
				 */
                    if (st != null) { // для 2-го блока try
                        st.close();
                    } else {
                        System.err.println("Statement was not created");
                    }
                }
        } catch(SQLException e) { // для 1-го блока try
            System.err.println("DB connection error: " + e);
					 /*

 * вивід повідомлення про всі SQLException
					 */
        } finally {
			/*
 * закрити Connection, якщо воно  було відкрите
			*/
            if (cn != null) {
                try {

                    cn.close();
                } catch (SQLException e) {

                    System.err.println("Сonnection close error: " + e);
                }
            }
        }
    }

}
