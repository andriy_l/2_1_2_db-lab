package sqlitedemo;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Enumeration;
import java.util.Properties;

public class InitDB {

    public static void main(String[] args) {
        Connection c2 = null;
        try {

            Properties properties = new Properties();
            try {
                properties.load(new FileInputStream("mydb.properties"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            String url = properties.getProperty("db.url");
            String user = properties.getProperty("db.user");
            String pass = properties.getProperty("db.password");
            c2 = DriverManager.getConnection(url, user, pass);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
}
